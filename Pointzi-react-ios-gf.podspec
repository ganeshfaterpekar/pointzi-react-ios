Pod::Spec.new do |spec|
  spec.name         = "Pointzi-react-ios-gf"
  spec.version      = "0.0.1"
  spec.summary      = "A short description of Pointzi-react-ios."
  spec.description  = <<-DESC
                    NetStatus is a small and lightweight Swift framework that allows to monitor and being notified for network status changes in a super-easy way!
                   DESC
  spec.homepage     = "https://appcoda.com"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "YOUR_NAME" => "YOUR_EMAIL" }
  spec.platform     = :ios, "8.0"
  spec.source       = { :git => "https://gitlab.com/ganeshfaterpekar/pointzi-react-ios.git" }
  spec.source_files = 'Pointzi-react-ios/*.{m,h}'
  spec.dependency "React"
end

