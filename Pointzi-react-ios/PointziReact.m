//
//  PointziReact.m
//  Pointzi-react-ios
//
//  Created by ganesh faterpekar on 4/30/20.
//  Copyright © 2020 Streethawk. All rights reserved.
//

#import "PointziReact.h"
#import "StreetHawkCore_Pointzi.h"
@implementation PointziReact

-(void) test {
    NSLog(@"In test vvv");
}

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(tagCuid:(NSString *)uniqueId)
{
  [StreetHawk tagCuid:uniqueId];
}

RCT_EXPORT_METHOD(tagUserLanguage:(NSString *)language)
{
  [StreetHawk tagUserLanguage:language];
}

RCT_EXPORT_METHOD(tagString:(NSString *)key value:(NSString *)value)
{
  [StreetHawk tagString:value forKey:key];
}

/**
 * Sends ViewName to Pointzi for Tips to target.
 * @param reactViewName name of View
 */
RCT_EXPORT_METHOD(viewWillRender:(NSString *)reactViewName)
{
     NSLog(@"viewWillRender");
    NSLog(@"%@",reactViewName);
    [StreetHawk setReactViewName:reactViewName];
}

RCT_EXPORT_METHOD(tagNumeric:(NSString *)key value:(double)value)
{
  [StreetHawk tagNumeric:value forKey:key];
}

RCT_EXPORT_METHOD(tagDatetime:(NSString *)key value:(NSString *)ISO8601DateString)
{
  NSDate *date = [RCTConvert NSDate:ISO8601DateString];
  [StreetHawk tagDatetime:date forKey:key];
}

RCT_EXPORT_METHOD(removeTag:(NSString *)key)
{
  [StreetHawk removeTag:key];
}

RCT_EXPORT_METHOD(incrementTag:(NSString *)key)
{
  [StreetHawk incrementTag:key];
}

RCT_EXPORT_METHOD(incrementTag:(NSString *)key value:(double)value)
{
  [StreetHawk incrementTag:value forKey:key];
}

@end




