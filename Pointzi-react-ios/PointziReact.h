//
//  PointziReact.h
//  Pointzi-react-ios
//
//  Created by ganesh faterpekar on 4/30/20.
//  Copyright © 2020 Streethawk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTConvert.h>

@interface PointziReact : NSObject <RCTBridgeModule>
-(void) test ;
@end


